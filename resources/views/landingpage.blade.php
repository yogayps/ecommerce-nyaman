<!DOCTYPE html>
<html lang="en">

	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<!-- SITE TITLE -->
		<title>E-Commerce Nyaman</title>			
		<!-- Latest Bootstrap min CSS -->
		<link rel="stylesheet" href="{{asset('alvida/css/bootstrap.min.css')}}">		
		<!-- Google Font -->
		<!-- <link href="http://fonts.googleapis.com/css?family=Cousine:400,700|Montserrat:400,700" rel="stylesheet" type="text/css"> -->
		<link rel="stylesheet" href="{{asset('alvida/css/font-google.css')}}">
		<!-- Font Awesome CSS -->
		<link rel="stylesheet" href="{{asset('alvida/css/font-awesome.min.css')}}">
		<!-- venobox -->
		<link rel="stylesheet" href="{{asset('alvida/css/venobox.css')}}" />		
		<!--- owl carousel Css-->
		<link rel="stylesheet" href="{{asset('alvida/css/owl.carousel.css')}}">
		<link rel="stylesheet" href="{{asset('alvida/css/owl.theme.css')}}">				
		<!-- animate CSS -->		
		<link rel="stylesheet" href="{{asset('alvida/css/animate.css')}}">		
		<!-- Style CSS -->
		<link rel="stylesheet" href="{{asset('alvida/css/style.css')}}">
		<!-- Font logo -->
		<!-- <link href='http://fonts.googleapis.com/css?family=Lobster+Two' rel='stylesheet' type='text/css'> -->
		<link rel="stylesheet" href="{{asset('alvida/css/font-google-logo.css')}}">
		<!-- CSS FOR COLOR SWITCHER -->
		<link rel="stylesheet" href="{{asset('alvida/css/switcher.css')}}"> 	
		<link rel="stylesheet" href="{{asset('alvida/css/style1.css')}}" id="colors">		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="{{asset('dashboard/css/icons.css') }}" rel="stylesheet" type="text/css" />
		<!-- Style Font Logo -->
		<style>
			.logo-font {
			  font: 400 40px/1.3 'Lobster Two', Helvetica, sans-serif;
			  color: #ff9900;	
			}
		</style>

	</head>
	
    <body data-spy="scroll" data-offset="80">

		<!-- START PRELOADER -->
		<div class="preloader">
			<div class="status">
				<div class="status-mes"><h4>E-Commerce Nyaman</h4></div>
			</div>
		</div>
		<!-- END PRELOADER -->
		
		<!-- START NAVBAR -->
		<div class="navbar navbar-default navbar-fixed-top menu-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="/"><img src="{{asset('alvida/img/logo3.png')}}"  alt="logo" class="img-responsive"></a>
				</div>
				<div class="navbar-collapse collapse">
					<nav>
						<ul class="nav navbar-nav navbar-right">
							<li><a class="page-scroll" href="#home"><!-- <i class="mdi mdi-home"></i> -->&nbsp;&nbsp;Home</a></li>
							<li><a class="page-scroll" href="#"><!-- <i class="mdi mdi-coffee"></i> -->&nbsp;&nbsp;Kopdar</a></li>
							<li><a class="page-scroll" href="{{asset('event-foto') }}"><!-- <i class="mdi mdi-calendar-check"></i> -->&nbsp;&nbsp;Kompetisi</a></li>
							<li><a class="page-scroll" href="#"><!-- <i class="mdi mdi-blogger"></i> -->&nbsp;&nbsp;Blog Edukasi & Ecommerce</a></li>
							<li><a class="page-scroll" href="#contact"><!-- <i class="mdi mdi-contact-mail"></i> -->&nbsp;&nbsp;Contact</a></li>
							<li><a class="page-scroll" href="#partner"><!-- <i class="mdi mdi-human-greeting"></i> -->&nbsp;&nbsp;Partner</a></li>					
						</ul>
					</nav>
				</div> 
			</div><!--- END CONTAINER -->
		</div> 
		<!-- END NAVBAR -->	

		<!-- START HOME -->
		<section id="home" class="welcome-area">
			<div class="welcome-slider-area">
				<div id="welcome-slide-carousel" class="carousel slide carousel-fade" data-ride="carousel">

					<ol class="carousel-indicators carousel-indicators-slider">
						<!-- <li data-target="#welcome-slide-carousel" data-slide-to="0" class="active"></li> -->
						 <a href="http://www.freepik.com"><span class="pull-right" style="/*margin-right: 0px;*/ font-size: 0.7em; color: white">Designed by ijeab / Freepik</span></a>
						<!-- <li data-target="#welcome-slide-carousel" data-slide-to="1"></li>
						<li data-target="#welcome-slide-carousel" data-slide-to="2"></li> -->
					</ol>
					
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="single-slide-item slide-1">
								<img src="{{asset('alvida/img/slide.jpg')}}" class="img-responsive" style="background: rgba(255, 255, 255, 0.36);">
								<!-- <div class="single-slide-item-table">
									<div class="single-slide-item-tablecell">
										<div class="container">
											<div class="row">
												<div class="col-md-12">
													<h2>Berbelanja online sangat mudah</h2>
													<p style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru exercit ation Lorem ipsum dolor sit amet.Veniam quis notru exercit.</p>
												</div>
											</div>
										</div>
									</div>
								</div> -->
							</div>
						</div>
						<!-- <div class="item">
							<div class="single-slide-item slide-2">
								<div class="single-slide-item-table">
									<div class="single-slide-item-tablecell">
										<div class="container">
											<div class="row">
												<div class="col-md-12">
													<h2>Pengiriman barang sangat cepat dan praktis</h2>
													<p style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru exercit ation Lorem ipsum dolor sit amet.Veniam quis notru exercit.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
						<!-- <div class="item">
							<div class="single-slide-item slide-3">
								<div class="single-slide-item-table">
									<div class="single-slide-item-tablecell">
										<div class="container">
											<div class="row">
												<div class="col-md-12">
													<h2>Berbelanja online sangat aman dan nyaman</h2>
													<p style="text-align: justify;">Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru exercit ation Lorem ipsum dolor sit amet.Veniam quis notru exercit.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</section>
		<!-- END  HOME DESIGN -->	

		<!-- START ABOUT -->
		<section id="about" class="about-us section-padding">
			<div class="container">
				<div class="row text-center">
					<div class="section-title wow zoomIn">
						<h2>e-Commerce nyAMAN</h2>
						<span></span>
						<div class="col-md-1 col-md-offset-1">
							<img src="{{asset('dashboard/images/logo-baru.png')}}" height="250">
						</div>
						<div class="col-md-10">
						<p style="text-align: justify;">Pilih nyaman dan aman belanja ketimbang bermacet ria di jalan dan ngantri di kasir…. Belanja online aja..  Yuk kita belanja dengan nyAMAN. Bulan kampanye ecommerce nyAMAN ini bertujuan untuk meningkatkan kesadaran kita semua melakukan transaksi online dengan benar sehingga kenyamanan bertransaksi bisa dirasakan oleh semua pihak.
						<br><br>
						Bulan Kampanye ecommerce nyAMAN ini akan diisi dengan kegiatan berbagi informasi tentang cara bertransaksi dan belanja online yang baik dan benar, baik melalui beragam social media, kegiatan kompetisi online dan juga kopdar para pelaku ecommerce Indonesia dalam ajang Seminar dan Pameran di dua kota besar Jakarta dan Makassar. Yuk ramaikan acara Bulan Kampanye ecommer nyAMAN ini, supaya makin nyAMAN belanjanya.
						</p>
						</div>
					</div>
				</div><!--- END ROW -->
			</div><!--- END CONTAINER -->
		</section>		
		<!-- END ABOUT -->		

		<!-- START SERVICES -->
		<section id="sosialisasi" class="our_services section-padding" style="background-image: url({{asset('alvida/img/service-bg.jpg')}}); background-size:cover; background-position: center center;background-attachment:fixed">
		   <div class="container">
				<div class="row text-center">
					<div class="section-title text-center wow zoomIn">
						<h2 class="section-title-white">Bulan Kampanye Ecommerce Nyaman</h2>
						<span class="section-title-white-span"></span>
						<p class="section-title-white" style="text-align: justify;">Bulan Kampanye ecommerce nyAMAN merupakan satu
						upaya meningkatkan kesadaran masyarakat akan ecommerce
						nyAMAN melalui beragam kegiatan yang mendekatkan para
						konsumen pada pemerintah dan juga marketplace serta eshop
						retail sehingga terjalin kepercayaan yang tinggi satu sama lain.</p>
					</div>						
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="service">
							<div class="icon"><img src="{{asset('dashboard/images/picture/kampanye.png')}}" height="60"></div>
							<a class="page-scroll" href="#kampanye"><h2>Kampanye</h2></a>
							<!-- <p style="text-align: justify;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p> -->
						</div>
					</div><!-- END COL -->
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="service">
							<div class="icon"><img src="{{asset('dashboard/images/picture/kopdar.png')}}" height="60"></div>
							<a href="#"><h2>Kopdar</h2></a>
							<!-- <p style="text-align: justify;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p> -->
						</div>
					</div><!-- END COL -->
					<div class="col-md-4 col-sm-12 col-xs-12">
						<div class="service">
							<div class="icon"><img src="{{asset('dashboard/images/picture/kompetisi.png')}}" height="60"></div>
							<a href="{{asset('event-foto') }}"><h2>Kompetisi</h2></a>
							<!-- <p style="text-align: justify;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p> -->
						</div>
					</div><!-- END COL -->
				</div><!-- END ROW -->
			</div><!-- END CONTAINER -->
		</section>
		<!-- END SERVICES -->		

		<!-- START ABOUT -->
		<section id="kampanye" class="about-us section-padding">
			<div class="container">
				<div class="row text-center">
					<div class="section-title wow zoomIn">
						<h2>​Edukasi​ ​e-commerce​ ​nyAMAN</h2>
						<span></span>
						<blockquote style="font-size: 14px">"​Dalam​ ​rangka​ ​Bulan​ ​Kampanye ecommerce​ ​nyAMAN,​ ​Kominfo​ ​bersama dengan​ ​partner​ ​ecommerce​ ​Indonesia, menghimbau​ ​para​ ​pengguna​ ​internet​ ​akan perlunya​ ​kesadaran​ ​untuk​ ​meningkatkan kenyamanan​ ​dalam​ ​ecommerce​ ​dengan​ ​cara memperhatikan​ ​keamanan​ ​transaksi."</blockquote>
						<a href="https://drive.google.com/drive/folders/1jrLt-6JhXfte0KNUkW23qEW68BYqhXik"><button class="btn btn-contact-bg">Download Materi</button></a>
						<br>
						<div class="col-md-12">​<h3>​Tips​ ​Belanja​ ​Online​ ​nyAMAN</h3></div>
						<div class="col-md-6">
							<h3>Video Animasi</h3>
							<div class="bg-info embed-responsive embed-responsive-16by9 text-white">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/WxoVCW2Xx-w?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
						</div>
						<div class="col-md-6">
							<h3>Poster</h3>
							<img src="{{asset('dashboard/images/picture/poster.jpg')}}" height="600" class="img-responsive">
						</div>
					</div>
				</div><!--- END ROW -->
			</div><!--- END CONTAINER -->
		</section>		
		<!-- END ABOUT -->	

		<!-- START HOME BLOG DESING  -->
		<section id="event" class="latest_blog section-padding">
			<div class="container">
				<div class="row">
					<div class="section-title text-center wow zoomIn">
						<h2>​Kompetisi​ ​Karya​ ​Kreatif​ ​ ​e-commerce​ ​nyAMAN</h2>
						<span></span>
						<p style="text-align: justify;">​Beragam​ ​kegiatan-kegiatan​ ​menarik​ ​selama​ ​bulan​ ​kampanye​ ​e-commerce nyaman​ ​diantara​ ​:
						</p>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-2">
						<div class="single_blog">
							<img src="dashboard/images/picture/poster-foto.jpg" alt="" class="img-responsive"/>
							<div class="blog-text wow fadeInLeft">
								 <!-- <span><i class="fa fa-eye"></i> 249 View</span><span><i class="fa fa-comments-o"></i> 08 Comment</span> -->
								 <a href="{{asset('event-foto') }}"><h4 class="text-center">Lomba Foto</h4></a>
								 <p style="text-align: justify;">​Ayo​ ​ciptakan​ ​foto​ ​kreatif​ ​kamu​ ​dengan​ ​tema​ ​e-commerce​ ​nyAMANyang​ ​meliputi​ ​kegiatan​ ​Keamanan,​ ​Kenyamanan,​ ​dan​ ​Kegemaranmu berbelanja​ ​online. 
								 </p>
							</div>
						</div><!--- END SINGLE BLOG POST -->
					</div><!--- END COL -->

					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="single_blog">
							<img src="dashboard/images/picture/poster-poster.jpg" alt="" class="img-responsive"/>
							<div class="blog-text wow fadeInLeft">
								 <!-- <span><i class="fa fa-eye"></i> 249 View</span><span><i class="fa fa-comments-o"></i> 08 Comment</span> -->
								 <a href="{{asset('event-poster') }}"><h4 class="text-center">Lomba Poster</h4></a>
								 <p style="text-align: justify;">​ ​Ayo​ ​ciptakan​ ​Poster​ ​kreatif​ ​kamu​ ​dengan​ ​tema​ ​e-commerce​ ​nyAMAN yang​ ​meliputi​ ​kegiatan​ ​Keamanan,​ ​Kenyamanan,​ ​dan​ ​Kegemaranmu berbelanja​ ​online.
								 </p>
							</div>
						</div><!--- END SINGLE BLOG POST -->
					</div><!--- END COL -->		
				</div><!--- END ROW -->	
			</div><!--- END CONTAINER-->
		</section>
		<!-- END HOME BLOG DESING  -->
		
		<!-- START TEAM -->
		<section id="partner" class="section-padding">
			<div class="container">
				<div class="row">
					<div class="section-title text-center wow zoomIn">
						<h2>Partner</h2>
						<span></span>
						<p style="text-align: justify;">Kesuksesan dari Bulan Kampanye ecommerce nyAMAN ini
						adalah hasil kerjasama dan sinergi antara KemKominfo dengan
						semua stakeholder ecommerce Indonesia, seperti Marketplace,
						eshop, retails dan pihak lain yang terkait.</p>
					</div>			
					<div class="col-md-3 col-sm-6 col-md-offset-1 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s" data-wow-offset="0">
						<div class="single_team">
							<div class="team_img">
								<center><img src="{{asset('alvida/img/partner/idea.jpg')}}" style="width: auto;" height="100" alt="" /></center>
							</div>
							<div class="team_text">
								<div class="team_text_inner">
									<h4>idEA</h4>
								</div>
							</div>
						</div>						
					</div><!--- END COL -->
					<div class="col-md-3 col-sm-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">
						<div class="single_team">
							<div class="team_img">
								<center><img src="{{asset('alvida/img/partner/xynexis.png')}}" style="width: 95%;" height="100" alt="" /></center>
							</div>
							<div class="team_text">
								<div class="team_text_inner">
									<h4>Xynexis</h4>							
								</div>
							</div>
						</div>						
					</div><!--- END COL -->						
					<div class="col-md-3 col-sm-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s" data-wow-offset="0">
						<div class="single_team">
							<div class="team_img">
								<center><img src="{{asset('alvida/img/partner/gunadarma.png')}}" style="width: 40%;" height="100" alt=""  /></center>
							</div>
							<div class="team_text">
								<div class="team_text_inner">
									<h4>Universitas​​ Gunadarma</h4>							
								</div>
							</div>
						</div>						
					</div><!--- END COL -->								
				</div><!--- END ROW -->
			</div><!--- END CONTAINER -->
		</section>
		<!-- END TEAM -->
		
		<!-- START CONTACT -->
		<section id="contact" class="contact_area section-padding" style="background-image: url({{asset('alvida/img/contact-bg.jpg')}}); background-size:cover; background-position: center center;background-attachment:fixed">
			<div class="container">				
				<div class="row">
					<div class="section-title text-center wow zoomIn">
						<h2 class="section-title-white">Contact Us</h2>
						<span class="section-title-white-span"></span>
						<p class="section-title-white" style="text-align: justify;">Kegiatan ini dapat terselenggara dengan adanya komunikasi yang lancar. Karenanya, mari kita berkomunikasi dengan baik.
						Untuk informasi tentang kegiatan dan program ini termasuk
						keinginan untuk turun memberikan dukungan pada kami,
						silahkan kontak: Unit Budaya Keamanan Informasi, UP;/ Ibu
						Poppy </p>
						<!-- budaya_ditkaminfo@kominfo.go.id -->
					</div>					
					<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0">
						<div class="contact">
							<form class="form" name="enq" method="post" action="contact.php" onsubmit="return validation();">
								<div class="row">
									<div class="form-group col-md-12">
										<input type="text" name="name" class="form-control" id="first-name" placeholder="Name" required="required">
									</div>
									<div class="form-group col-md-12">
										<input type="email" name="email" class="form-control" id="email" placeholder="Email" required="required">
									</div>
									<div class="form-group col-md-12">
										<textarea rows="6" name="message" class="form-control" id="description" placeholder="Your Message Here ..." required="required"></textarea>
									</div>
									<div class="form-group col-md-12 mb0">
									   <div class="actions">
										<input type="submit" value="Send Message" name="submit" id="submitButton" class="btn btn-lg btn-contact-bg" title="Click here to submit your message!" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div><!-- END COL -->
				</div><!--- END ROW -->				
			</div><!--- END CONTAINER -->		
		</section>
		<!-- END CONTACT  -->
		
		<!-- START CONTACT ADDRESS -->
		<div id="sosmed" class="contact-address">
			<div class="container">
				<div class="row text-center">
				<div class="section-title text-center wow zoomIn">
						<h2 class="section-title-white">Our Social Media</h2>
						<span class="section-title-white-span"></span>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single_address">
						<a href="https://www.facebook.com/ecommerce.nyAMAN" target="blank">
							<i class="fa fa-facebook"></i>
							<p>Ecommerce.nyaman</p>
						</a>
						</div>
					</div><!-- END COL  -->
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single_address">
						<a href="https://www.twitter.com/nyAMANecommerce" target="blank">
							<i class="fa fa-twitter"></i>
							<p>@nyAMANecommerce</p>
						</a>
						</div>
					</div><!-- END COL  -->
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single_address">
						<a href="https://www.instagram.com/ecommerce.nyaman" target="blank">
							<i class="fa fa-instagram"></i>
							<p>@ecommerce.nyaman</p>
						</a>
						</div>
					</div><!-- END COL  -->
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="single_address">
						<a href="https://goo.gl/4TSUUF" target="blank">
							<i class="fa fa-youtube"></i>
							<p>ecommerce nyAman</p>
						</a>
						</div>
					</div><!-- END COL  -->
				</div><!--END  ROW  -->
			</div><!-- END CONTAINER  -->
		</div>
		<!-- END CONTACT ADDRESS -->

		<!-- START FOOTER TOP -->
		<section class="footer section-padding">
			<div class="container">
				<div class="row">					
					<div class="col-md-12 col-sm-12 text-center wow zoomIn">
						<!-- <div class="footer_logo">
							<a href="/"> <img src="{{asset('alvida/img/logo2a.png')}}" height="60"> </a>
						</div> -->
						<div class="footer_social">
							<ul>
								<li><a class="f_facebook" href="https://www.facebook.com/ecommerce.nyAMAN"><i class="fa fa-facebook"></i></a></li>
								<li><a class="f_twitter" href="https://www.twitter.com/nyAMANecommerce"><i class="fa fa-twitter"></i></a></li>
								<li><a class="f_instagram" href="https://www.instagram.com/ecommerce.nyaman"><i class="fa fa-instagram"></i></a></li>
								<li><a class="f_youtube" href="https://goo.gl/4TSUUF"><i class="fa fa-youtube"></i></a></li>
							</ul>
						</div>
						<div class="copyright">
							<p>Copyright © 2017. E-Commerce Nyaman <a href="#">E-Commerce Nyaman</a>  |   All Rights Reserved
							<br><strong>KEMKOMINFO (Kementerian Komunikasi dan Informatika)</strong>
                                    <br>
                                    Jalan Medan Merdeka Barat No. 9
                                    Jakarta Pusat 10110
                                    Jakarta, Indonesia</p>
						</div><!--- END FOOTER COPYRIGHT -->
					</div><!--- END COL -->			
				</div><!--- END ROW -->				
			</div><!--- END CONTAINER -->
		</section>
		<!-- END FOOTER TOP -->		
		 
		<!-- Latest jQuery -->
			<script src="{{asset('alvida/js/jquery-1.12.4.min.js')}}"></script>
		<!-- Latest compiled and minified Bootstrap -->
			<script src="{{asset('alvida/js/bootstrap.min.js')}}"></script>
		<!-- modernizer JS -->		
			<script src="{{asset('alvida/js/modernizr-2.8.3.min.js')}}"></script>				
		<!-- venobox js -->
			<script src="{{asset('alvida/js/venobox.min.js')}}"></script>
		<!-- jquery appear js  -->
			<script src="{{asset('alvida/js/jquery.appear.js')}}"></script>			
		<!-- countTo js -->
			<script src="{{asset('alvida/js/jquery.inview.min.js')}}"></script>			
		<!-- jquery mixitup js -->
			<script src="{{asset('alvida/js/jquery.mixitup.js')}}"></script>			
		<!-- owl-carousel min js  -->
			<script src="{{asset('alvida/js/owl.carousel.min.js')}}"></script>				
		<!-- scrolltopcontrol js -->
			<script src="{{asset('alvida/js/scrolltopcontrol.js')}}"></script>					
		<!-- WOW - Reveal Animations When You Scroll -->
			<script src="{{asset('alvida/js/wow.min.js')}}"></script>
		<!-- form-contact js -->
			<script src="{{asset('alvida/js/form-contact.js')}}"></script>			
		<!-- map js -->
			<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwIQh7LGryQdDDi-A603lR8NqiF3R_ycA"></script> -->
			<script src="{{asset('alvida/js/google-map.js')}}"></script>
		<!-- switcher js -->
			<script src="{{asset('alvida/js/switcher.js')}}"></script>			
		<!-- scripts js -->
			<script src="{{asset('alvida/js/scripts.js')}}"></script>
    </body>
</html>