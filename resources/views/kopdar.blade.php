@extends('layout/layout')

@section('after-styles')
<style type="text/css">
    .shadowcard-box {
            padding: 10px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            -moz-border-radius: 3px;
            background-clip: padding-box;
            margin-bottom: 20px;
            background-color: #ffffff;
        }

    li.active a{
        color: red;
    }

    /*COUNTDOWN*/

    #clockdiv{
        font-family: sans-serif;
        color: #000;
        display: inline-block;
        font-weight: 100;
        text-align: center;
        font-size: 15px;
    }

    #clockdiv > div{
        padding: 10px;
        border-radius: 3px;
        background: #EFEFEF;
        display: inline-block;
    }

    #clockdiv div > span{
        padding: 15px;
        border-radius: 3px;
        background: #BABABA;
        display: inline-block;
    }

    .smalltext{
        padding-top: 5px;
        font-size: 15px;
    }

    p{
        text-align: justify;
    }
</style>

        <!--venobox lightbox-->
        <link rel="stylesheet" href="{{asset('dashboard/plugins/magnific-popup/css/magnific-popup.css') }}"/>
@endsection

@section('content')
                <div class="">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="p-20">
                                <div class="shadowcard-box">
                                    <div class="blog-post m-b-30">
                                            <div class="post-image">
                                                <img src="dashboard/images/blog/1.jpg" alt="" class="img-responsive">
                                                <span class="label label-danger">Lifestyle</span>
                                            </div>
                                            <div class="text-muted"><span>by <a class="text-dark font-secondary">John Doe</a>,</span> <span>Sep 10, 2016</span></div>
                                            <div class="post-title">
                                                <h3><a href="javascript:void(0);">Exclusive: Get a First Look at the Fall Collection</a></h3>
                                            </div>
                                            <hr>
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                                            <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                        </div>
                                </div>
                            </div>
                        </div> <!-- end col -->

                        <div class="col-sm-4">
                            <div class="p-20">
                                <div class="m-t-0">
                                    <h4 class="text-uppercase">Daftar Kompetisi</h4>
                                    <div class="border m-b-20"></div>

                                    <ul class="blog-categories-list list-unstyled">
                                        <li><a href="{{ ('event-foto') }}"><i class="fa fa-camera"></i>&nbsp;&nbsp;Lomba Foto</a></li>
                                        <li><a href="{{ ('event-vlog') }}"><i class="fa-video-camera"></i>&nbsp;&nbsp;Lomba Vlog</a></li>
                                        <li><a href="{{ ('event-poster') }}"><i class="fa fa-file-image-o"></i>&nbsp;&nbsp;Lomba Poster</a></li>
                                        <li><a href="{{ ('event-infografis') }}"><i class="fa fa-paint-brush"></i>&nbsp;&nbsp;Lomba Infografis</a></li>
                                        <li><a href="{{ ('event-instagram') }}"><i class="fa fa-instagram"></i>&nbsp;&nbsp;Lomba Instagram</a></li>
                                    </ul>
                                </div>

                                <div class="m-t-50">
                                    <h4 class="text-uppercase">Facebook Pages</h4>
                                    <div class="border m-b-20"></div>

                                    <div class="fb-post" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/" data-width="500" data-show-text="true"><blockquote cite="https://www.facebook.com/20531316728/posts/10154009990506729/" class="fb-xfbml-parse-ignore">Dikirim oleh <a href="https://www.facebook.com/facebook/">Facebook</a> pada&nbsp;<a href="https://www.facebook.com/20531316728/posts/10154009990506729/">27 Agustus 2015</a></blockquote></div>
                                </div>

                                <div class="m-t-50">
                                    <h4 class="text-uppercase">Instagram Pages</h4>
                                    <div class="border m-b-20"></div>

                                    <blockquote class="instagram-media" data-instgrm-version="2" style=" background:#000000; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"><div style=" background:#000000; line-height:0; margin-top:40px; padding-bottom:55%; padding-top:45%; text-align:center; width:100%;"><div style="position:relative;"><div style=" -webkit-animation:dkaXkpbBxI 1s ease-out infinite; animation:dkaXkpbBxI 1s ease-out infinite; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-44px; width:44px;"></div><span style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:12px; font-style:normal; font-weight:bold; position:relative; top:15px;">Loading</span></div></div><p style=" line-height:32px; margin-bottom:0; margin-top:8px; padding:0; text-align:center;"> <a href="https://www.instagram.com/p/Baa1LPaDiTL/?taken-by=kemenkominfo" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a></p></div><style>@-webkit-keyframes"dkaXkpbBxI"{ 0%{opacity:0.5;} 50%{opacity:1;} 100%{opacity:0.5;} } @keyframes"dkaXkpbBxI"{ 0%{opacity:0.5;} 50%{opacity:1;} 100%{opacity:0.5;} }</style></blockquote><script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                                </div>

                                <div class="m-t-50">
                                    <h4 class="text-uppercase">Social Media</h4>
                                    <div class="border m-b-20"></div>
                                    <center>
                                    <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-instagram"></i> </button>
                                    <button class="btn btn-icon waves-effect waves-light btn-purple m-b-5"> <i class="fa fa-yahoo"></i> </button>
                                    <button class="btn btn-icon waves-effect waves-light btn-primary m-b-5"> <i class="fa fa-facebook-official"></i> </button>
                                    <button class="btn btn-icon waves-effect waves-light btn-info m-b-5"> <i class="fa fa-twitter"></i> </button>
                                    <button class="btn btn-icon waves-effect waves-light btn-warning m-b-5"> <i class="fa fa-google-plus"></i> </button>
                                    </center>
                                </div>
                                <div class="m-b-20">
                                </div>

                            </div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
@endsection

@section('after-scripts')

    <script type="text/javascript">
        function getTimeRemaining(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor((t / 1000) % 60);
          var minutes = Math.floor((t / 1000 / 60) % 60);
          var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
          var days = Math.floor(t / (1000 * 60 * 60 * 24));
          return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
          };
        }

        function initializeClock(id, endtime) {
          var clock = document.getElementById(id);
          var daysSpan = clock.querySelector('.days');
          var hoursSpan = clock.querySelector('.hours');
          var minutesSpan = clock.querySelector('.minutes');
          var secondsSpan = clock.querySelector('.seconds');

          function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
              clearInterval(timeinterval);
            }
          }

          updateClock();
          var timeinterval = setInterval(updateClock, 1000);
        }

        var deadline = new Date(Date.parse(new Date()) + 24 * 24 * 60 * 60 * 1000);
        initializeClock('clockdiv', deadline);
    </script>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=262552117101150";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection