<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <title>E-Commerce Nyaman | Admin Panel</title>

        <!-- Summernote css -->
        <link href="{{asset('dashboard/plugins/summernote/summernote.css')}}" rel="stylesheet" />

        <!-- Select2 -->
        <link href="{{asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Jquery filer css -->
        <link href="{{asset('dashboard/plugins/jquery.filer/css/jquery.filer.css')}}" rel="stylesheet" />
        <link href="{{asset('dashboard/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" rel="stylesheet" />

        <!-- App css -->
        <link href="{{asset('dashboard/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{asset('dashboard/plugins/switchery/switchery.min.css') }}">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{asset('dashboard/js/modernizr.min.js') }}"></script>
<style type="text/css">
    hr.style {
    background-color: #fff;
    border-top: 2px dashed #8c8b8b;
}

.footer { background: #222 }
.footer_logo img {
    margin-bottom: 30px;
    width: auto;
}
/*START FOOTER SOCIAL DESIGN*/
.footer_social { margin-bottom: 5px }
.footer_social ul {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
.footer_social ul li { display: inline-block }
.footer_social ul li a {
    border: 1px solid #333;
    border-radius: 30px;
    color: #fff;
    display: block;
    font-size: 14px;
    height: 40px;
    line-height: 20px;
    margin: 2px;
    padding: 9px 12px;
    -webkit-transition: all 0.2s ease 0s;
    transition: all 0.2s ease 0s;
    width: 40px;
}
@media only screen and (max-width:768px) { 
    .footer_social ul li a { font-size: 12px }
}
@media only screen and (max-width:360px) { 
    .footer_social ul li a { font-size: 14px }
}
.footer_social ul li a:hover { color: #fff }
.f_facebook:hover {
    background: #5D82D1;
    border: 1px solid #5D82D1;
}
.f_twitter:hover {
    background: #40BFF5;
    box-shadow: 0 0 0 0px #40BFF5;
    border: 1px solid #40BFF5;
}
.f_google:hover {
    background: #EB5E4C;
    box-shadow: 0 0 0 0px #EB5E4C;
    border: 1px solid #EB5E4C;
}
.f_linkedin:hover {
    background: #238CC8;
    box-shadow: 0 0 0 0px #238CC8;
    border: 1px solid #238CC8;
}
.f_youtube:hover {
    background: #CC181E;
    box-shadow: 0 0 0 0px #CC181E;
    border: 1px solid #CC181E;
}
.f_skype:hover {
    background: #00AFF0;
    box-shadow: 0 0 0 0px #00AFF0;
    border: 1px solid #00AFF0;
}
.f_instagram:hover {
    background: #b73aa8;
    box-shadow: 0 0 0 0px #b73aa8;
    border: 1px solid #b73aa8;
}
/*END FOOTER SOCIAL DESIGN*/
.copyright p {
    border-top: 1px solid #1f2428;
    font-size: 14px;
    color: #eee;
    margin-bottom: 0;
    text-align: center;
}
.copyright a {
    color: #03a9f4;
    font-family: "Cousine",sans-serif;
    -webkit-transition: all 0.2s ease 0s;
    transition: all 0.2s ease 0s;
}
.copyright a:hover { color: #fff }
</style>
    </head>


    <body>


        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main" style="background-color: #31aaba">
                <div class="container">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!--<a href="index.html" class="logo">-->
                            <!--Zircos-->
                        <!--</a>-->
                        <!-- Image Logo -->
                        <a href="index.html" class="logo">
                            <img src="dashboard/images/logonyaman.png" alt="" height="50">
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li class="navbar-c-items">
                                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                                     <input type="text" placeholder="Search..." class="form-control">
                                     <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>
                    <!-- end menu-extras -->

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu pull-right">

                            <li class="has-submenu">
                                <a href="{{asset('landingpage') }}"><!-- <i class="mdi mdi-home"></i> -->Home</a>
                            </li>

                            <li class="has-submenu">
                                <a href="{{asset('kopdar') }}"><!-- <i class="mdi mdi-coffee"></i> -->Kopdar</a>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><!-- <i class="mdi mdi-calendar-check"> --></i>Kompetisi</a>
                                <ul class="submenu">
                                    <li><a href="{{ ('event-foto') }}">Lomba Foto</a></li>
                                    <li><a href="{{ ('event-vlog') }}">Lomba Vlog</a></li>
                                    <li><a href="{{ ('event-poster') }}">Lomba Poster</a></li>
                                    <li><a href="{{ ('event-infografis') }}">Lomba Infografis</a></li>
                                    <!-- <li><a href="{{ ('event-instagram') }}">Lomba Instagram</a></li> -->
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="{{ ('dashboard-edukasi') }}"><!-- <i class="mdi mdi-blogger"></i> -->Blog Edukasi & E-commerce</a>
                            </li>

                            <li class="has-submenu">
                                <a href="{{asset('landingpage#contact') }}"><!-- <i class="mdi mdi-contact-mail"></i> -->Contact</a>
                            </li>

                            <li class="has-submenu">
                                <a href="{{asset('landingpage#partner') }}"><!-- <i class="mdi mdi-human-greeting"></i> -->Partner</a>
                            </li>
                            <li class="has-submenu">
                                <a href="#" style="color: red"><!-- <i class="mdi mdi-account-circle" style="color: red"></i> -->Admin Panel</a>
                                <ul class="submenu">
                                    <li><a href="{{ ('blog-postingfoto') }}">Tambah/Edit Lomba Foto</a></li>
                                    <li><a href="{{ ('blog-postingvlog') }}">Tambah/Edit Lomba Vlog</a></li>
                                    <li><a href="{{ ('blog-postingposter') }}">Tambah/Edit Lomba Poster</a></li>
                                    <li><a href="{{ ('blog-postinginfografis') }}">Tambah/Edit Lomba Infografis</a></li>
                                    <!-- <li><a href="{{ ('blog-postinginstagram') }}">Tambah/Edit Lomba Instagram</a></li> -->
                                </ul>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li>
                                        <a href="#">Admin Panel</a>
                                    </li>
                                    <li class="active">
                                        Tambah / Ubah Post (Lomba Foto)
                                    </li>
                                </ol>
                            </div>
                            <h4 class="page-title"><a href="">Tambah</a> / <a href="#">Ubah Post</a></h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->


                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="p-20">
                            <div class="">
                                <div class="card-box" style="background-color: #31aaba; border-radius: 10px;">
                                    <h3 class="text-center" style="color: white"><i class="mdi mdi-camera"></i>&nbsp;&nbsp;Tambah Post (Lomba Foto)</h3>
                                </div>
                                <form role="form">
                                    <div class="form-group m-b-20">
                                        <h4 for="exampleInputEmail1">Judul Event</h4>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter title">
                                    </div>
                                    <hr class="style">
                                    <div class="form-group m-b-20">
                                        <h4>Upload Gambar</h4>
                                        <input type="file" name="files[]" id="filer_input1"
                                                       multiple="multiple">
                                    </div>
                                    <hr class="style">
                                    <div class="form-group m-b-20">
                                        <h4>Description <font color="red">(Mekanisme)</font></h4>
                                        <div class="summernote">
                                        </div>
                                    </div>
                                    <hr class="style">
                                    <div class="form-group m-b-20">
                                        <h4>Description <font color="red">(Syarat Ketentuan)</font></h4>
                                        <div class="summernote">
                                        </div>
                                    </div>
                                    <hr class="style">
                                    <div class="form-group m-b-20">
                                        <h4>Description <font color="red">(Hadiah)</font></h4>
                                        <div class="summernote">
                                        </div>
                                    </div>
                                    <hr class="style">
                                    <div class="form-group m-b-20">
                                        <label>Kategori post</label>
                                        <select class="select2 form-control select2-multiple" multiple="multiple" data-placeholder="Choose ...">
                                            <option value="lombafoto">Lomba Foto</option>
                                            <option value="lombainfografis">Lomba Infografis</option>
                                            <!-- <option value="lombainstagram">Lomba Instagram</option> -->
                                            <option value="lombaposter">Lomba Poster</option>
                                            <option value="lombavlog">Lomba Vlog</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success waves-effect waves-light">Simpan dan Tampilkan</button>
                                    <button type="button" class="btn btn-danger waves-effect waves-light">Batal</button>
                                </form>
                            </div>
                        </div> <!-- end p-20 -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->



                <!-- Footer -->
<div style="margin-bottom: 90px">
        <section class="footer section-padding">
            <div class="container">
                <div class="row">                   
                    <div class="col-md-12 col-sm-12 text-center wow zoomIn">
                        <!-- <div class="footer_logo">
                            <a href="/"> <img src="{{asset('alvida/img/logo2a.png')}}" height="60"> </a>
                        </div> -->
                        <div class="footer_social">
                            <ul>
                                <li><a class="f_facebook" href="https://www.facebook.com/ecommerce.nyAMAN"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="f_twitter" href="https://www.twitter.com/nyAMANecommerce"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="f_instagram" href="https://www.instagram.com/ecommerce.nyaman"><i class="fa fa-instagram"></i></a></li>
                                <li><a class="f_youtube" href="https://goo.gl/4TSUUF"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                        <div class="copyright">
                            <p>Copyright © 2017. E-Commerce Nyaman <a href="#">E-Commerce Nyaman</a>  |   All Rights Reserved
                            <br>
                            <strong>KOMINFO (Kementerian Komunikasi dan Informatika)</strong>
                                    <br>
                                    Jalan Medan Merdeka Barat No. 9
                                    Jakarta Pusat 10110
                                    Jakarta, Indonesia</p>
                        </div><!--- END FOOTER COPYRIGHT -->
                    </div><!--- END COL -->         
                </div><!--- END ROW -->             
            </div><!--- END CONTAINER -->
        </section>
    </div>
                <!-- End Footer -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- jQuery  -->
        <script src="{{asset('dashboard/js/jquery.min.js') }}"></script>
        <script src="{{asset('dashboard/js/bootstrap.min.js') }}"></script>
        <script src="{{asset('dashboard/js/detect.js') }}"></script>
        <script src="{{asset('dashboard/js/fastclick.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.blockUI.js') }}"></script>
        <script src="{{asset('dashboard/js/waves.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.slimscroll.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{asset('dashboard/plugins/switchery/switchery.min.js') }}"></script>

        <!--Summernote js-->
        <script src="{{asset('dashboard/plugins/summernote/summernote.min.js')}}"></script>
        <!-- Select 2 -->
        <script src="{{asset('dashboard/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>
        <!-- Jquery filer js -->
        <script src="{{asset('dashboard/plugins/jquery.filer/js/jquery.filer.min.js')}}"></script>

        <!-- page specific js -->
        <script src="{{asset('dashboard/pages/jquery.blog-add.init.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/jquery.core.js')}}"></script>
        <script src="{{asset('dashboard/js/jquery.app.js')}}"></script>

        <script>

            jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 240,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
                // Select2
                $(".select2").select2();

                $(".select2-limiting").select2({
                    maximumSelectionLength: 2
                });
            });
        </script>


    </body>
</html>