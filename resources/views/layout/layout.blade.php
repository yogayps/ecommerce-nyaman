<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <title>E-Commerce Nyaman</title>

        <!-- App css -->
        <link href="{{asset('dashboard/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{asset('dashboard/css/responsive.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{asset('dashboard/plugins/switchery/switchery.min.css') }}">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        @yield('after-styles')

        <script src="{{asset('dashboard/js/modernizr.min.js') }}"></script>

    </head>


    <body>

        @include('layout/header')

        <div class="wrapper">
            <div class="container">
            
                    @yield('content')

            </div>
        </div>

        @include('layout/footer')

    
        <!-- jQuery  -->
        <script src="{{asset('dashboard/js/jquery.min.js') }}"></script>
        <script src="{{asset('dashboard/js/bootstrap.min.js') }}"></script>
        <script src="{{asset('dashboard/js/detect.js') }}"></script>
        <script src="{{asset('dashboard/js/fastclick.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.blockUI.js') }}"></script>
        <script src="{{asset('dashboard/js/waves.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.slimscroll.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{asset('dashboard/plugins/switchery/switchery.min.js') }}"></script>

        <!-- App js -->
        <script src="{{asset('dashboard/js/jquery.core.js') }}"></script>
        <script src="{{asset('dashboard/js/jquery.app.js') }}"></script>


        @yield('after-scripts')
        
    </body>
        
</html>

