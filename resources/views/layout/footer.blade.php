
<style type="text/css">
.footer { background: #222 }
.footer_logo img {
    margin-bottom: 30px;
    width: auto;
}
/*START FOOTER SOCIAL DESIGN*/
.footer_social { margin-bottom: 5px }
.footer_social ul {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
.footer_social ul li { display: inline-block }
.footer_social ul li a {
    border: 1px solid #333;
    border-radius: 30px;
    color: #fff;
    display: block;
    font-size: 14px;
    height: 40px;
    line-height: 20px;
    margin: 2px;
    padding: 9px 12px;
    -webkit-transition: all 0.2s ease 0s;
    transition: all 0.2s ease 0s;
    width: 40px;
}
@media only screen and (max-width:768px) { 
    .footer_social ul li a { font-size: 12px }
}
@media only screen and (max-width:360px) { 
    .footer_social ul li a { font-size: 14px }
}
.footer_social ul li a:hover { color: #fff }
.f_facebook:hover {
    background: #5D82D1;
    border: 1px solid #5D82D1;
}
.f_twitter:hover {
    background: #40BFF5;
    box-shadow: 0 0 0 0px #40BFF5;
    border: 1px solid #40BFF5;
}
.f_google:hover {
    background: #EB5E4C;
    box-shadow: 0 0 0 0px #EB5E4C;
    border: 1px solid #EB5E4C;
}
.f_linkedin:hover {
    background: #238CC8;
    box-shadow: 0 0 0 0px #238CC8;
    border: 1px solid #238CC8;
}
.f_youtube:hover {
    background: #CC181E;
    box-shadow: 0 0 0 0px #CC181E;
    border: 1px solid #CC181E;
}
.f_skype:hover {
    background: #00AFF0;
    box-shadow: 0 0 0 0px #00AFF0;
    border: 1px solid #00AFF0;
}
.f_instagram:hover {
    background: #b73aa8;
    box-shadow: 0 0 0 0px #b73aa8;
    border: 1px solid #b73aa8;
}
/*END FOOTER SOCIAL DESIGN*/
.copyright p {
    border-top: 1px solid #1f2428;
    font-size: 14px;
    color: #eee;
    margin-bottom: 0;
    text-align: center;
}
.copyright a {
    color: #03a9f4;
    font-family: "Cousine",sans-serif;
    -webkit-transition: all 0.2s ease 0s;
    transition: all 0.2s ease 0s;
}
.copyright a:hover { color: #fff }
</style>

    <div style="margin-bottom: 90px">
        <section class="footer section-padding">
            <div class="container">
                <div class="row">                   
                    <div class="col-md-12 col-sm-12 text-center wow zoomIn">
                        <!-- <div class="footer_logo">
                            <a href="/"> <img src="{{asset('alvida/img/logo2a.png')}}" height="60"> </a>
                        </div> -->
                        <div class="footer_social">
                            <ul>
                                <li><a class="f_facebook" href="https://www.facebook.com/ecommerce.nyAMAN"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="f_twitter" href="https://www.twitter.com/nyAMANecommerce"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="f_instagram" href="https://www.instagram.com/ecommerce.nyaman"><i class="fa fa-instagram"></i></a></li>
                                <li><a class="f_youtube" href="https://goo.gl/4TSUUF"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                        <div class="copyright">
                            <p>Copyright © 2017. E-Commerce Nyaman <a href="#">E-Commerce Nyaman</a>  |   All Rights Reserved
                            <br>
                            <strong>KEMKOMINFO (Kementerian Komunikasi dan Informatika)</strong>
                                    <br>
                                    Jalan Medan Merdeka Barat No. 9
                                    Jakarta Pusat 10110
                                    Jakarta, Indonesia</p>
                        </div><!--- END FOOTER COPYRIGHT -->
                    </div><!--- END COL -->         
                </div><!--- END ROW -->             
            </div><!--- END CONTAINER -->
        </section>
    </div>
                <!-- Footer -->
                <!-- <div class="m-t-30">
                <footer class="footer" style="background-color: #fe7125;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 text-center" style="color: white">
                                Copyright © 2017. E-Commerce Nyaman
                                <div class="row">
                                    <p style="font-size: 0.8em" class="text-center"><strong>KOMINFO (Kementerian Komunikasi dan Informatika)</strong>
                                    <br>
                                    Jalan Medan Merdeka Barat No. 9
                                    Jakarta Pusat 10110
                                    Jakarta, Indonesia</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                </div> -->
                <!-- End Footer -->