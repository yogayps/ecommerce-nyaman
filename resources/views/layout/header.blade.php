<style type="text/css">
</style>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main" style="background: #31aaba;">
                <div class="container">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!--<a href="index.html" class="logo">-->
                            <!--Zircos-->
                        <!--</a>-->
                        <!-- Image Logo -->
                        <a href="index.html" class="logo">
                            <img src="dashboard/images/logonyaman.png" alt="" height="50">
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li class="navbar-c-items">
                                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                                     <input type="text" placeholder="Search..." class="form-control">
                                     <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>
                    <!-- end menu-extras -->

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu pull-right">

                            <li class="has-submenu">
                                <a href="{{asset('landingpage') }}"><!-- <i class="mdi mdi-home"></i> -->Home</a>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><!-- <i class="mdi mdi-coffee"></i> -->Kopdar</a>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><!-- <i class="mdi mdi-calendar-check"></i> -->Kompetisi</a>
                                <ul class="submenu">
                                    <li><a href="{{ ('event-foto') }}">Lomba Foto</a></li>
                                    <!-- <li><a href="{{ ('event-vlog') }}">Lomba Vlog</a></li> -->
                                    <li><a href="{{ ('event-poster') }}">Lomba Poster</a></li>
                                    <!-- <li><a href="{{ ('event-infografis') }}">Lomba Infografis</a></li> -->
                                    <!-- <li><a href="{{ ('event-instagram') }}">Lomba Instagram</a></li> -->
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"><!-- <i class="mdi mdi-blogger"></i> -->Blog Edukasi & E-commerce</a>
                            </li>

                            <li class="has-submenu">
                                <a href="{{asset('landingpage#contact') }}"><!-- <i class="mdi mdi-contact-mail"></i> -->Contact</a>
                            </li>

                            <li class="has-submenu">
                                <a href="{{asset('landingpage#partner') }}"><!-- <i class="mdi mdi-human-greeting"></i> -->Partner</a>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->