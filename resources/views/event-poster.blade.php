@extends('layout/layout')

@section('after-styles')
<style type="text/css">
    .shadowcard-box {
            padding: 10px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            -moz-border-radius: 3px;
            background-clip: padding-box;
            margin-bottom: 20px;
            background-color: #ffffff;
        }

    li.active a{
        color: red;
    }

    /*COUNTDOWN*/

    #clockdiv{
        font-family: sans-serif;
        color: #ffffff;
        display: inline-block;
        font-weight: 100;
        text-align: center;
        font-size: 20px;
    }

    #clockdiv > div{
        padding: 10px;
        border-radius: 3px;
        display: inline-block;
    }

    #clockdiv div > span{
        padding: 15px;
        border-radius: 3px;
        display: inline-block;
    }

    .smalltext{
        padding-top: 5px;
        font-size: 15px;
    }

    p {
        text-align: justify;
    }

    li {
        text-align: justify;
    }

    hr.style {
    background-color: #fff;
    border-top: 2px dashed #8c8b8b;
}

/*CONTACT AREA*/
.contact_area{position:relative;}
.contact_area:before{
    background: rgba(0, 0, 0, 0.8);
    content: "";
    height: 100%;
    left: 0;
    opacity: 0.8;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
    position: absolute;
    top: 0;
    width: 100%;

}
.contact {
background: #fff;
padding: 60px;
}
.form-control {
    background: none;
    border: 1px solid #e8e8e9;
    border-radius: 0;
    box-shadow: none;
    height: 45px;
    font-family: "Roboto Slab",sans-serif;
    -webkit-transition: all 0.4s ease 0s;
            transition: all 0.4s ease 0s;
}
.form-control:focus {
color:#333;
border: 1px solid #03a9f4;
    background:none;
    box-shadow: none;
    outline: 0 none;
}
.btn-contact-bg {
background: #03a9f4 none repeat scroll 0 0;
border-radius: 0;
color: #fff;
padding: 10px 40px;
text-transform: uppercase;
-webkit-transition: all 0.4s ease 0s;
transition: all 0.4s ease 0s;
}
.btn-contact-bg:hover,
.btn-contact-bg:focus {
    background: #fe7125;
    border: 1px solid #fe7125;
    color: #fff;
}
.mb0 { margin-bottom: 0 }
/*END CONTACT*/
</style>

        <!--venobox lightbox-->
        <link rel="stylesheet" href="{{asset('dashboard/plugins/magnific-popup/css/magnific-popup.css') }}"/>
@endsection

@section('content')
                <div class="">
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-2">
                            <div class="p-20">
                                    <h1 class="text-center" style="color: #31aaba">Karya​ ​Kreatif​ ​e-commerce​ ​nyAMAN</h1>
                                    <h3 class="text-center" style="color: #fe7125"><i class="fa fa-file-image-o"></i>&nbsp;&nbsp;​LOMBA POSTER</h3>
                                <hr class="style">
                                <p>Kompetisi​ ​Poster​ ​ini​ ​mengangkat​ ​tema​ ​tentang​ ​e-commerce​ ​di​ ​Indonesia.​ ​Kompetisi​ ​Poster​ ​ini memiliki​ ​tema​ ​besar​ ​yaitu​ ​“​e-commerce​ ​nyAMAN​”,​ ​dengan​ ​sub​ ​tema​ ​yaitu​ ​:
                                <ol type="a">
                                <li>Keamanan​ ​transaksi​ ​ecommerce​ ​di​ ​Indonesia</li>
                                <li>Kenyamanan​ ​berbelanja​ ​online​ ​di​ ​Indonesia</li>
                                <li>Kegemaran​ ​dan​ ​Keceriaan​ ​berbelanja​ ​online​ ​di​ ​Indonesia</li>
                                </ol>
                                </p>
                                <hr class="style">
                                <div class="thumb">
                                    <a href="dashboard/images/picture/poster-poster.jpg" class="image-popup" title="Screenshot-2">
                                        <img src="dashboard/images/picture/poster-poster.jpg" class="thumb-img" alt="work-thumbnail">
                                    </a>
                                </div>
                                <div class="card-box" style="background: black">
                                    <div class="row">
                                        <center>
                                        <h4 class="text-center">COUNTDOWN EVENT</h4>
                                        <div id="clockdiv">
                                              <div>
                                                <span class="days"></span>
                                                <div class="smalltext"><b>Days</b></div>
                                              </div>
                                              <div>
                                                <span class="hours"></span>
                                                <div class="smalltext"><b>Hours</b></div>
                                              </div>
                                              <div>
                                                <span class="minutes"></span>
                                                <div class="smalltext"><b>Minutes</b></div>
                                              </div>
                                              <div>
                                                <span class="seconds"></span>
                                                <div class="smalltext"><b>Seconds</b></div>
                                              </div>
                                        </div>
                                        </center>
                                    </div>
                                </div>
                                <hr class="style">
                                <h3 class="text-center" style="color: #fe7125">KETENTUAN DAN PERSYARATAN LOMBA</h3>
                                <div class="shadowcard-box text-center">
                                    <br>
                                    <ul class="nav nav-tabs navtab-bg nav-justified">
                                        <li class="">
                                            <a href="#ketentuan" data-toggle="tab" aria-expanded="false">
                                                <i class="mdi mdi-settings "></i>&nbsp;&nbsp;Ketentuan Lomba
                                            </a>
                                        </li>
                                        <li class="active">
                                            <a href="#persyaratan" data-toggle="tab" aria-expanded="true">
                                                <i class=" mdi mdi-bell-outline "></i>&nbsp;&nbsp;Persyaratan Peserta
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#hadiah" data-toggle="tab" aria-expanded="false">
                                                <i class=" mdi mdi-package-variant"></i>&nbsp;&nbsp;Hadiah
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane" id="ketentuan">
                                            <p>
                                            <ol type="A">
                                            <li>Poster​ ​yang​ ​diupload​ ​harus​ ​sesuai​ ​dengan​ ​tema​ ​kompetisi​ ​yaitu 
                                            “ecommerce​ ​nyAMAN”​​ ​yaitu​ ​kegiatan​ ​dalam​ ​:  
                                                <ol type="a">
                                                <li>Keamanan​ ​transaksi​ ​ecommerce​ ​di​ ​Indonesia </li>
                                                <li>Kenyamanan​ ​berbelanja​ ​online​ ​di​ ​Indonesia </li>
                                                <li>Kegemaran​ ​dan​ ​Keceriaan​ ​berbelanja​ ​online​ ​di​ ​Indonesia </li>
                                                </ol></li>
                                            <li>Peserta​ ​bebas​ ​menggunakan​ ​aplikasi​ ​editing​ ​apapun. </li>
                                            <li>Batas​ ​akhir​ ​upload​ ​karya​ ​adalah​ ​18​ ​November​ ​2017​ ​pukul​ ​24:00​ ​WIB </li>
                                            <li>Ukuran​ ​Poster​ ​yaitu​ ​A2.  </li>
                                            <li>Peserta​ ​wajib​ ​memfollow​ ​akun​ ​instagram​ ​@ecommerce.nyAMAN </li> 
                                            <li>Peserta​ ​wajib​ ​repost/regram​ ​banner​ ​lomba​ ​Poster. </li>
                                            <li>Tidak​ ​ada​ ​batasan​ ​dalam​ ​maksimum​ ​jumlah​ ​Poster​ ​yang​ ​diupload </li>
                                            <li>Upload​ ​dan​ ​mention​ ​akun​ ​@ecommerce.nyAMAN​ ​beserta​ ​tag​ ​5​ ​temanmu​. </li>
                                            <li>Upload​ ​menggunakan​ ​hastag​ #
                                             ​ ecommerce.nyAMAN.poster. </li>
                                            <li>Poster​ ​tidak​ ​mengandung​ ​SARA,​ ​pornografi,​ ​ujaran​ ​kebencian,​ ​dan​ ​tidak 
                                            melanggar​ ​hukum/aturan​ ​berlaku. </li>
                                            </ol>
                                            </p>
                                        </div>
                                        <div class="tab-pane active" id="persyaratan">
                                            <p>
                                            <ol type="A">
                                            <li>Kegiatan​ ​ini​ ​terbuka​ ​untuk​ ​seluruh​ ​Warga​ ​Negara​ ​Indonesia​ ​yang​ ​memiliki 
                                            kartu​ ​identitas​ ​diri​ ​yang​ ​valid </li>
                                            <li>Peserta​ ​tidak​ ​dipungut​ ​biaya. </li>
                                            <li>Lomba​ ​berlaku​ ​untuk​ ​umum​ ​(jurnalis,blogger,​ ​PNS,​ ​mahasiswa,dll)​ ​kecuali 
                                            pegawai​ ​kementrian​ ​Komunikasi​ ​dan​ ​Informatika. </li>
                                            <li>Kementerian​ ​Komunikasi​ ​dan​ ​Informatika​ ​Republik​ ​Indonesia​ ​berhak 
                                            menggunakan​ ​Poster-Poster​ ​pemenang​ ​untuk​ ​kegiatan​ ​promosi,​ ​publikasi 
                                            dan​ ​keperluan​ ​lainnya​ ​tanpa​ ​memberikan​ ​kompensasi​ ​apapun​ ​kepada 
                                            pemenang​ ​tersebut​ ​dengan​ ​hak​ ​cipta​ ​Poster​ ​tetap​ ​dipegang​ ​oleh​ ​pemilik 
                                            Poster.​ ​Poster​ ​pemenang​ ​harus​ ​bebas​ ​dari​ ​paten,​ ​hak​ ​cipta​ ​atau​ ​ikatan​ ​hak 
                                            lainnya,​ ​dan​ ​jika​ ​Poster​ ​tersebut​ ​terikat​ ​pada​ ​suatu​ ​hak​ ​cipta​ ​atau​ ​hak 
                                            lainnya,​ ​maka​ ​pemenang​ ​menjamin​ ​dan​ ​memastikan​ ​Kementerian 
                                            Komunikasi​ ​dan​ ​Informatika​ ​Republik​ ​Indonesia​ ​bebas​ ​dari​ ​segala​ ​tuntutan 
                                            apapun​ ​dari​ ​pihak​ ​mana​ ​pun​ ​dan​ ​segala​ ​bentuk​ ​kewajiban​ ​yang​ ​timbul​ ​akan 
                                            menjadi​ ​tanggungan​ ​pemenang. </li>
                                            <li>Poster​ ​bersifat​ ​original​ ​dan​ ​terbaru,​ ​tidak​ ​melanggar​ ​hak​ ​kekayaan 
                                            intelektual​ ​pihak​ ​manapun,​ ​dan​ ​belum​ ​pernah​ ​dipublikasikan​ ​sebelumnya. </li>
                                            <li>Panitia​ ​berhak​ ​mendiskualifikasi​ ​secara​ ​sepihak​ ​para​ ​peserta​ ​yang​ ​dianggap 
                                            melanggar​ ​syarat,​ ​ketentuan​ ​dan​ ​peraturan​ ​Kementerian​ ​Komunikasi​ ​dan 
                                            Informatika​ ​Republik​ ​Indonesia​ ​tanpa​ ​perlu​ ​menyertakan​ ​alasan​ ​apapun. </li>
                                            <li>Keputusan​ ​dewan​ ​juri​ ​adalah​ ​mutlak​ ​dan​ ​tidak​ ​dapat​ ​diganggu​ ​gugat. </li>
                                            <li>Dengan​ ​melakukan​ ​pendaftaran​ ​di​ ​kegiatan​ ​ini,​ ​peserta​ ​menyatakan​ ​telah 
                                            membaca,​ ​mengerti​ ​dan​ ​setuju​ ​dengan​ ​segala​ ​peraturan​ ​beserta​ ​syarat​ ​dan 
                                            ketentuan​ ​lomba​ ​Poster​ ​yang​ ​diselenggarakan​ ​oleh​ ​Kementerian​ ​Komunikasi 
                                            dan​ ​Informatika​ ​Republik​ ​Indonesia. </li>
                                            <li>Poster​ ​tidak​ ​mengandung​ ​SARA,​ ​pornografi,​ ​ujaran​ ​kebencian,​ ​dan​ ​tidak 
                                            melanggar​ ​hukum/aturan​ ​berlaku.  </li>
                                            <li>Apalbila​ ​di​ ​kemudian​ ​hari​ ​ditemukan​ ​pelanggaran​ ​terhadap​ ​syarat​ ​dan 
                                            ketentuan​ ​tersebut​ ​diatas.​ ​Dewan​ ​Juri​ ​berhak​ ​melakukan​ ​diskualifikasi​ ​dan 
                                            membuat​ ​ketentuan​ ​lebih​ ​lanjut. </li>
                                            </ol>
                                            </p>
                                        </div>
                                        <div class="tab-pane" id="hadiah">
                                            <p>
                                            <ol type="a">
                                            <li>Finalis​ ​dan​ ​Pemenang​ ​lomba​ ​mendapat​ ​sertifikat​ ​dari​ ​Kementerian 
                                            Komunikasi​ ​dan​ ​Informatika​ ​Republik​ ​Indonesia. </li>
                                            <li>Karya​ ​Poster​ ​dari​ ​12​ ​finalis​ ​lomba​ ​akan​ ​dicetak​ ​kedalam​ ​Kalender​ ​2018 
                                            Kemkominfo. </li>
                                            <li>Kemkominfo​ ​mempublikasikan​ ​karya​ ​finalis​ ​dan​ ​pemenang​ ​lomba​ ​melalui 
                                            berbagai​ ​media​ ​yang​ ​dimiliki​ ​oleh​ ​Kemkominfo. </li>
                                            <li>Karya​ ​Poster​ ​12​ ​finalis​ ​akan​ ​dipamerkan​ ​pada​ ​event​ ​offline​ ​di​ ​Makassar. </li>
                                           <li> Beragam​ ​Hadiah​ ​menarik​ ​dari​ ​stakeholder/marketplace​ ​e-commerce.</li>
                                            </ol>
                                            </p>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                                <hr class="style">
                            </div>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->
                    <div class="row">
                        <div class="col-sm-4 col-md-offset-4">
                            <div class="shadowcard-box">
                                    <h4 class="text-uppercase text-center">Instagram Embed</h4>
                                    <div class="border m-b-20"></div>

                                    <blockquote class="instagram-media" data-instgrm-version="2" style=" background:#000000; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"><div style=" background:#000000; line-height:0; margin-top:40px; padding-bottom:55%; padding-top:45%; text-align:center; width:100%;"><div style="position:relative;"><div style=" -webkit-animation:dkaXkpbBxI 1s ease-out infinite; animation:dkaXkpbBxI 1s ease-out infinite; background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-44px; width:44px;"></div><span style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:12px; font-style:normal; font-weight:bold; position:relative; top:15px;">Loading</span></div></div><p style=" line-height:32px; margin-bottom:0; margin-top:8px; padding:0; text-align:center;"> <a href="https://www.instagram.com/p/Baa1LPaDiTL/?taken-by=kemenkominfo" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a></p></div><style>@-webkit-keyframes"dkaXkpbBxI"{ 0%{opacity:0.5;} 50%{opacity:1;} 100%{opacity:0.5;} } @keyframes"dkaXkpbBxI"{ 0%{opacity:0.5;} 50%{opacity:1;} 100%{opacity:0.5;} }</style></blockquote><script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-2">
                            <hr class="style">
                                            <h2 class="text-center">Contact Us</h2>
                                            <p style="text-align: justify;">Kegiatan ini dapat terselenggara dengan adanya komunikasi yang lancar. Karenanya, mari kita berkomunikasi dengan baik.
                                            Untuk informasi tentang kegiatan dan program ini termasuk
                                            keinginan untuk turun memberikan dukungan pada kami,
                                            silahkan kontak: Unit Budaya Keamanan Informasi, UP;/ Ibu
                                            Poppy
                                            <!-- budaya_ditkaminfo@kominfo.go.id -->
                                            </p>     

                                        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                            <div class="contact">
                                                <form class="form" name="enq" method="post" action="contact.php" onsubmit="return validation();">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <input type="text" name="name" class="form-control" id="first-name" placeholder="Name" required="required">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="required">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <textarea rows="6" name="message" class="form-control" id="description" placeholder="Your Message Here ..." required="required"></textarea>
                                                        </div>
                                                        <div class="form-group col-md-12 mb0">
                                                           <div class="actions">
                                                            <input type="submit" value="Send Message" name="submit" id="submitButton" class="btn btn-lg btn-contact-bg" title="Click here to submit your message!" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div><!-- END COL -->
                        </div>
                    </div>
                </div>
@endsection

@section('after-scripts')

        <!-- isotope filter plugin -->
        <script type="text/javascript" src="dashboard/plugins/isotope/js/isotope.pkgd.min.js"></script>

        <!-- Magnific popup -->
        <script type="text/javascript" src="dashboard/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

        <!-- form-contact js -->
        <script src="{{asset('alvida/js/form-contact.js')}}"></script> 
        
        <script type="text/javascript">
            $(window).load(function(){
                var $container = $('.portfolioContainer');
                $container.isotope({
                    filter: '*',
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });

                $('.portfolioFilter a').click(function(){
                    $('.portfolioFilter .current').removeClass('current');
                    $(this).addClass('current');

                    var selector = $(this).attr('data-filter');
                    $container.isotope({
                        filter: selector,
                        animationOptions: {
                            duration: 750,
                            easing: 'linear',
                            queue: false
                        }
                    });
                    return false;
                });
            });
            $(document).ready(function() {
                $('.image-popup').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    mainClass: 'mfp-fade',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    }
                });
            });
        </script>

    <script type="text/javascript">
        function getTimeRemaining(endtime) {
          var t = Date.parse(endtime) - Date.parse(new Date());
          var seconds = Math.floor((t / 1000) % 60);
          var minutes = Math.floor((t / 1000 / 60) % 60);
          var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
          var days = Math.floor(t / (1000 * 60 * 60 * 24));
          return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
          };
        }

        function initializeClock(id, endtime) {
          var clock = document.getElementById(id);
          var daysSpan = clock.querySelector('.days');
          var hoursSpan = clock.querySelector('.hours');
          var minutesSpan = clock.querySelector('.minutes');
          var secondsSpan = clock.querySelector('.seconds');

          function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
              clearInterval(timeinterval);
            }
          }

          updateClock();
          var timeinterval = setInterval(updateClock, 1000);
        }

        var deadline = new Date(Date.parse(new Date()) + 24 * 24 * 60 * 60 * 1000);
        initializeClock('clockdiv', deadline);
    </script>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=262552117101150";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
@endsection