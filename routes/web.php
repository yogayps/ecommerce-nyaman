<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Landing Page*/
Route::get('/', 'LandingpageController@LandingPage')->name('landingpage');

/*Origin*/
Route::get('/origin', function () {
    return view('origin');
});

Route::get('landingpage', 'LandingpageController@LandingPage')->name('landingpage');

Route::get('event-foto', 'EventFotoController@EventFoto')->name('event-foto');

Route::get('event-vlog', 'EventVlogController@EventVlog')->name('event-vlog');

Route::get('event-poster', 'EventPosterController@EventPoster')->name('event-poster');

Route::get('event-infografis', 'EventInfografisController@EventInfografis')->name('event-infografis');

Route::get('event-instagram', 'EventInstagramController@EventInstagram')->name('event-instagram');

Route::get('dashboard-edukasi', 'DashboardEdukasiController@DashboardEdukasi')->name('dashboard-edukasi');

Route::get('kopdar', 'KopdarController@Kopdar')->name('kopdar');

Route::get('blog-postingfoto', 'BlogPostingFotoController@BlogPostingFoto')->name('blog-postingfoto');

Route::get('blog-postinginfografis', 'BlogPostingInfografisController@BlogPostingInfografis')->name('blog-postinginfografis');

Route::get('blog-postinginstagram', 'BlogPostingInstagramController@BlogPostingInstagram')->name('blog-postinginstagram');

Route::get('blog-postingposter', 'BlogPostingPosterController@BlogPostingPoster')->name('blog-postingposter');

Route::get('blog-postingvlog', 'BlogPostingVlogController@BlogPostingVlog')->name('blog-postingvlog');