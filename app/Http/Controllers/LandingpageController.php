<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingpageController extends Controller
{
    public function LandingPage(Request $request){
        return view('landingpage');
    }
}
